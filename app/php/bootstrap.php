<?php

/**
 * SasaApp ws
 *
 */

require 'dbconn.php';

#region variables

/**
 * action selector
 * @var string
 */
$do = (isset($_REQUEST['req'])) ?  $_REQUEST['req'] : '' ;

/**
 * phone number 254...
 * @var string
 */
$phone = (isset($_REQUEST['phone'])) ?  $_REQUEST['phone'] : '' ;

/**
 * name
 * @var string
 */
$name = (isset($_REQUEST['name'])) ?  $_REQUEST['name'] : '' ;

/**
 * code
 * @var string
 */
$code = (isset($_REQUEST['code'])) ?  $_REQUEST['code'] : '' ;

/**
 * log code
 * @var string
 */
$logcode = (isset($_REQUEST['logcode'])) ?  $_REQUEST['logcode'] : '' ;

/**
 * status
 * @var string
 */
$status = (isset($_REQUEST['status'])) ?  $_REQUEST['status'] : '' ;

/**
 * image
 * @var string
 */
$image = (isset($_REQUEST['image'])) ?  $_REQUEST['image'] : '' ;

 /**
 * password
 * @var string
 */
 $password;

#endregion


#region switch

switch ($do) {

	case 'register' : doRegister($phone);
	break;
	
	case 'login' : doLogin($phone);
	break;

	case 'enter-code' : doVerifyCode($phone, $code);
	break;
	
	case 'login-enter-code' : doLoginVerifyCode($logcode);
	break;

	case 'create-profile' : doCreateProfile($phone, $name, $status, $image);
	break;

	case 'get-profile' : doGetProfile($phone);
	break;

	case 'get-auth' : doGetAuth($phone);
	break;

	case 'update-name' : doUpdateName($phone, $name);
	break;

	case 'update-status' : doUpdateName($phone, $status);
	break;

	case 'update-image' : doUpdateImage($phone, $image);
	break;

	case 'reset-password' : doResetPassword($phone);
	break;

	default : doSendCode('254714990734', '4525');//echo doGeneratePassword(64);
	break;

}

#endregion

#region callback_functions

/**
 * Begin user registration
 *
 * @param 	int		$phone 	phone number
 * @return 	bool 	$ret 	1 / 0	
 */
function doRegister($phone) {

	global $db;

	// insert into db
	try {
		
		$code = doGenerateCode(4);

		$stmt = $db->prepare("INSERT INTO tbl_users (user_phoneno, user_code) VALUES (:phone, :code) ON DUPLICATE KEY UPDATE user_code = :code_up ");
		$stmt->bindParam(':phone', $phone);
		$stmt->bindParam(':code', $code);
		$stmt->bindParam(':code_up', $code);


		$stmt->execute();

		$num = $stmt->rowCount();

		if ($num > 0) {

			echo 1;
			doSendCode($phone, $code);

		} else {

			echo 0;

		}

	} catch(PDOException $e) {

		doHandleException($e);

	}

}
/**
 * Web Phone No. Login 
 *
 * @param 	int   	$phone 	user phone number
 * @param 	int 	$code 	4 digit code	
 * @return 	bool 	$ret 	1 / 0
 */
function doLogin($phone) {

	global $db;
	
	try{	
	
		$code = doGenerateCode(4);
		
		$phon = trim($phone, "+");
		
		$stmt = $db->prepare("UPDATE tbl_users SET user_code = :code WHERE user_phoneno = :phone");
		$stmt->bindParam(':phone', $phon);
		$stmt->bindParam(':code', $code);
		
		$stmt->execute();
		
		$num = $stmt->rowCount();

		if ($num > 0) {

			echo 1;
			doSendCode($phone, $code);

		} else {

			echo 0;

		}

	} catch(PDOException $e) {

		doHandleException($e);

	}

}

/**
 * Verify code 
 *
 * @param 	int   	$phone 	user phone number
 * @param 	int 	$code 	4 digit code	
 * @return 	bool 	$ret 	1 / 0
 */
function doVerifyCode($phone, $code) { 

	global $db;

	// get the code
	try {

		$stmt = $db->prepare("SELECT user_code FROM tbl_users WHERE user_phoneno = :phone LIMIT 1");
		$stmt->bindParam(':phone', $phone);

		$stmt->execute();

		$result = $stmt->fetchAll(); 

		if (count($result)) {

			$db_code = $result[0]['user_code'];

			if ($db_code == $code) {

				// set user password
				$pwd = doSetUserPassword($phone); 

				if ($pwd) {

					echo 1;

				} else {

					echo 0;

				}

			} else {

				echo 0;

			}

		} else {

			echo 0;

		}

	} catch(PDOException $e) {

		doHandleException($e);

	}

}

/**
 * Web Login Verify code 
 *
 * @param 	int   	$phone 	user phone number
 * @param 	int 	$code 	4 digit code	
 * @return 	bool 	$ret 	1 / 0
 */
function doLoginVerifyCode($logcode) { 

	global $db;

	// get the code
	try {

		$stmt = $db->prepare("SELECT * FROM tbl_users WHERE user_code = :smscode LIMIT 1");
		$stmt->bindParam(':smscode', $logcode);

		$stmt->execute();

		$res = $stmt->fetch(PDO::FETCH_ASSOC); 

		if ($res) {
			
			$json[] = array( 'id' => $res['id'], 'ph' => $res['user_phoneno'], 'pw' =>  $res['user_password'] );
			
			echo json_encode($json);

		} else {

			$json[] = array( 'res' => 'none' );
			
			echo json_encode($json);

		}

	} catch(PDOException $e) {

		doHandleException($e);

	}

}

/**
 * Create user profile
 *
 * @param 	int   	$phone 	user phone number
 * @param 	string 	$name	user name
 * @return 	bool 	$ret 	1 / 0
 */
function doCreateProfile($phone, $name, $status, $image) {

	global $db;

	// insert into db
	try {

		$stmt = $db->prepare('INSERT INTO tbl_userprofile (up_phoneno, up_name, up_status, up_profilepic, up_profilemessage) VALUES (:phone, :name, :status, :image, :message)');
		$stmt->bindParam(':phone', $phone);
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':status', $status);
		$stmt->bindParam(':image', $image);
		$stmt->bindParam(':message', $status);

		$stmt->execute();

		$num = $stmt->rowCount();

		if ($num > 0) {

			echo 1;

		} else {

			echo 0;

		}

	} catch (PDOException $e) {

		doHandleException($e);

	}

}

/**
 * Get xmpp auth details
 *
 * @param 	int   	$phone 	user phone number
 * @return 	json 	$ret 	
 */
function doGetAuth($phone) {

	global $db;

	// fetch from db
	try {

		$stmt = $db->prepare('SELECT user_jid, user_password FROM tbl_users WHERE user_phoneno = :phone LIMIT 1');
		$stmt->bindParam(':phone', $phone);

		$stmt->execute();

		$result = $stmt->fetchAll();
		$xmpp_config = doGetXmppConfig();

		if (count($result)) {

			$ret[] = array(
				'username' => $result[0]['user_jid'],
				'password' => $result[0]['user_password'],
				'host' => $xmpp_config[0]['host'],
				'port' => $xmpp_config[0]['port']
			);

			$payload['payload'] = $ret;

			echo json_encode($payload);

		} else {

			$ret[] = array(
				'username' => '',
				'password' => '',
				'host' => '',
				'port' => ''
			);

			$payload['payload'] = $ret;

			echo json_encode($payload);

		}

	} catch(PDOException $e) {

		doHandleException($e);

	}

}

/**
 * Gets a user's profile
 *
 * @param 	int 	$phone 		user phone
 * @return 	json 	$payload 	json encoded user profile object
 */
function doGetProfile($phone) {

	global $db;

	// fetch from db
	try {
	
		$stmt = $db->prepare('SELECT * FROM tbl_userprofile WHERE up_phoneno = :phone');
		$stmt->bindParam(':phone', $phone);

		$stmt->execute();

		$result = $stmt->fetchAll();

		if (count($result)) {

			$ret[] = array(
				'username' => $result[0]['up_name'],
				'status' => $result[0]['up_status'],
				'photo' => $result[0]['up_profilepic'],
				'phone' => $result[0]['up_phoneno']
			);

			$payload['payload'] = $ret;

			echo json_encode($payload);

		} else {

			$ret[] = array(
				'username' => '',
				'status' => '',
				'photo' => '',
				'phone' => ''
			);

			$payload['payload'] = $ret;

			echo json_encode($payload);

		}

	} catch(PDOException $e) {

		doHandleException($e);

	}

}

/**
 * Update user name
 *
 * @param 	string  $phone 	user phone
 * @param 	string  $name 	user name	
 * @return 	bool 	$ret 	1 / 0
 */
function doUpdateName($phone, $name) {
	
	global $db;

	// insert into db
	try {

		$stmt = $db->prepare('UPDATE tbl_userprofile SET up_name = :name WHERE up_phoneno = :phone');
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':phone', $phone);
		
		$stmt->execute();

		$num = $stmt->rowCount();

		if ($num > 0) {

			echo 1;

		} else {

			echo 0;

		}

	} catch (PDOException $e) {

		doHandleException($e);

	}

}

/**
 * Update status
 *
 * @param 	string  $phone 	user phone
 * @param 	string  $status user status	
 * @return 	bool 	$ret 	1 / 0
 */
function doUpdateSatus($phone, $status) {

	global $db;

	// insert into db
	try {

		$stmt = $db->prepare('UPDATE tbl_userprofile SET up_status = :status WHERE up_phoneno = :phone');
		$stmt->bindParam(':status', $status);
		$stmt->bindParam(':phone', $phone);

		$stmt->execute();

		$num = $stmt->rowCount();

		if ($num > 0) {

			echo 1;

		} else {

			echo 0;

		}

	} catch (PDOException $e) {

		doHandleException($e);

	}

}

/**
 * Update image
 *
 * @param 	string  $phone 	user phone
 * @param 	blob    $image 	user image	
 * @return 	bool 	$ret 	1 / 0
 */
function doUpdateImage($phone, $image) {

	global $db;

	// insert into db
	try {

		$stmt = $db->prepare('UPDATE tbl_userprofile SET up_profilepic = :image WHERE up_phoneno = :phone');
		$stmt->bindParam(':image', $image);
		$stmt->bindParam(':phone', $phone);

		$stmt->execute();

		$num = $stmt->rowCount();

		if ($num > 0) {

			echo 1;

		} else {

			echo 0;

		}

	} catch (PDOException $e) {

		doHandleException($e);

	}

}

/**
 * reset password
 *
 * @param 	string  $phone 	user phone
 * @return 	bool 	$ret 	1 / 0
 */
function doResetPassword($phone) {

	global $db;

	// insert into db
	try {

		$password = doGeneratePassword();

		$stmt = $db->prepare('UPDATE tbl_users SET user_password = :password WHERE user_phoneno = :phone');
		$stmt->bindParam(':password', $password);
		$stmt->bindParam(':phone', $phone);

		$stmt->execute();

		$num = $stmt->rowCount();

		if ($num > 0) {

			echo 1;

		} else {

			echo 0;

		}

	} catch (PDOException $e) {

		doHandleException($e);

	}

}

#endregion

#region generators

/**
 * Generate a numeric code
 *
 * @param 	void
 * @param 	int 	$len 	code length
 * @return 	int 	$code  	numeric code	
 */
function doGenerateCode($len = 4) {

	$seed = '0123456789';
	$code = '';

	for ($i = 0; $i < $len; $i++) {

		$j = mt_rand(0, 9);
		$code .= $seed[$j];

	}

	return $code;

}

/**
 * Generate user password
 *
 * @param 	int 	$len 	password length
 * @return 	string 	$paswd 	user password	
 */
function doGeneratePassword($len = 64) {

	$seed = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-+=|{[]}?';
	$paswd = '';

	for ($i = 0; $i < $len; $i++) {

		$j = mt_rand(0, (strlen($seed) - 1));
		$paswd .= $seed[$j];

	}

	return $paswd;

}

#endregion

#region utilities 

/**
 * Send code to phone via sms
 *
 * @param 	int   	$phone 	user phone number
 * @param 	int 	$code 	4 digit code	
 * @return 	void
 */
function doSendCode($phone, $code) {

	require_once 'AfricasTalkingGateway.php';

	$username = 'jaymo';
	$apikey = 'b0c7fee87b9ccb63478f115222d6cd475b647b528af11ee2cb93df43ed5a69ad';
	$recepient = '+' . trim($phone);

	$message = 'Welcome to SasaApp, your code is: ' . trim($code);

	$gateway = new AfricasTalkingGateway($username, $apikey);

	$ret = array();

	try {

		$results = $gateway->sendMessage($recepient, $message);

		foreach ($results as $result) {
			
			$ret = array(
				'number' => $result->number,
				'message' => $message,
				'status' => $result->status,
				'message_id' => $result->messageId,
				'cost' => $result->cost
			);

		}

		doSmsLog($ret['number'], $ret['message'], $ret['message_id'], $ret['cost'], $ret['status']);

	} catch(AfricasTalkingGatewayException $e) {

		doHandleException($e);;

	}

}

/**
 * Register xmpp user
 * Insert user details in tbl_xmpp_registration,
 *
 * @param 	int   	$phone 	user phone number
 * @param 	int 	$paswd 	user password
 * @return 	bool 	$ret 	
 */
function doXmppRegistration($phone, $paswd) {

	global $db;

	// insert into db
	try {

		$state = 0;
		$jid = trim($phone) . '@sasaapp';

		$stmt = $db->prepare('INSERT INTO tbl_xmpp_registration (user_phone, user_password, user_registration_state, user_jid) VALUES (:phone, :password, :user_registration_state, :user_jid)');
		$stmt->bindParam(':phone', $phone);
		$stmt->bindParam(':password', $paswd);
		$stmt->bindParam(':user_registration_state', $state);
		$stmt->bindParam(':user_jid', $jid);
		
		$stmt->execute();

		$num = $stmt->rowCount();

		if ($num > 0) {

			return 1;

		} else {

			return 0;

		}

	} catch (PDOException $e) {

		doHandleException($e);

	}

}

/**
 *
 *
 */
function doSetUserPassword($phone) {

	global $db;

	// insert into db
	try {

		$password = doGeneratePassword();
		$jid = trim($phone) . '@sasaapp';


		$stmt = $db->prepare('UPDATE tbl_users SET user_password = :password, user_jid = :jid WHERE user_phoneno = :phone');
		$stmt->bindParam(':password', $password);
		$stmt->bindParam(':phone', $phone);
		$stmt->bindParam(':jid', $jid);
		
		$stmt->execute();

		$num = $stmt->rowCount();

		if ($num > 0) {

			$xpass = doXmppRegistration($phone, $password);

			if ($xpass) {

				return 1;

			} else {

				return 0;

			}

		} else {

			return 0;

		}

	} catch (PDOException $e) {

		doHandleException($e);

	}

}

/**
 *
 *
 */
function doGetXmppConfig() {

	$config[] = array(
		'host' => '192.168.0.126',
		'port' => '5220',
		'domain' => 'sasaapp'
	);

	return $config;

}

/**
 * Handle error
 *
 * @param 	object 	$e 		error object
 * @return 	bool 	$ret 	
 */
function doHandleException($e) {

	echo $e->getMessage();

}

/**
 *
 *
 */
function doSmsLog($number, $message, $message_id, $cost, $status) {

	global $db;

	// insert into db
	try {

		$stmt = $db->prepare('INSERT INTO tbl_sms_log (phone, message, message_id, cost, status) VALUES (:phone, :message, :message_id, :cost, :status)');
		$stmt->bindParam(':phone', $number);
		$stmt->bindParam(':message', $message);
		$stmt->bindParam(':message_id', $message_id);
		$stmt->bindParam(':cost', $cost);
		$stmt->bindParam(':status', $status);
		
		$stmt->execute();

		$num = $stmt->rowCount();
		
	} catch (PDOException $e) {

		doHandleException($e);

	}

}

#endregion