<?php

#region db_connect

$db_host = 'localhost';
$db_name = 'sasaapp';
$db_user = 'root';
$db_pass = 'root';

try {

	$db = new PDO("mysql:host=$db_host;dbname=$db_name;", $db_user, $db_pass);

} catch(PDOException $e) {

	echo $e->getMessage();

}

#endregion

?>