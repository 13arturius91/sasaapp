# Sasa App 
Sasa App is a chat application with wide range of communication features available (such as one-to-one messaging, group chat messaging, file transfers and audio/video calls).

## It includes features such as:

1. Welcome page
2. Log in page 
3. Main page
4. Contact list
5. Chat page
6. Group chat page
7. Audio calls (WebRTC)
8. Video calls (WebRTC)
9. My profile page
10. Сontact profile page
11. Local Search
12. File transfer via chat
13. Chat emojes
14. Icons and sound on a browser's tab
15. Full screen mode for audio and video calls

## Changelog

### v. 0.1.0.9 – July 29, 2015
* Style and UI bug fixes
* Dialog.js, session.js, message.js fix

### v. 0.1.0.5 – July 16, 2015
* Bug fixes for chat.js

### v. 0.1.1 – July 13, 2015
* Style fixes, Login and Sign Up by phone number

### v. 0.1.0 – July 3, 2015
* Bug  fixes of the chat session expiration